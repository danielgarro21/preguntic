/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ulatina.preguntic.controller;

import edu.ulatina.preguntic.model.Rol;
import java.io.Serializable;
import java.sql.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Daniel Garro
 */

@ManagedBean(name = "bibliotecaController")
@SessionScoped
public class BibliotecaController implements Serializable{
   private int id;
   private int idUsuarioCarnet;
   private String nombrel;
   private String tipoArchivo;
   private Date fechaSubida;
   private Date fechaDeEliminacion;
   private double tamano;
   private String derechosDeAutor;
   private String licencias;
   private String enlace;
   
    public BibliotecaController(int id, int idUsuarioCarnet, String nombrel, String tipoArchivo, Date fechaSubida, Date fechaDeEliminacion, double tamano, String derechosDeAutor, String licencias, String enlace) {
        this.id = id;
        this.idUsuarioCarnet = idUsuarioCarnet;
        this.nombrel = nombrel;
        this.tipoArchivo = tipoArchivo;
        this.fechaSubida = fechaSubida;
        this.fechaDeEliminacion = fechaDeEliminacion;
        this.tamano = tamano;
        this.derechosDeAutor = derechosDeAutor;
        this.licencias = licencias;
        this.enlace = enlace;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUsuarioCarnet() {
        return idUsuarioCarnet;
    }

    public void setIdUsuarioCarnet(int idUsuarioCarnet) {
        this.idUsuarioCarnet = idUsuarioCarnet;
    }

    public String getNombrel() {
        return nombrel;
    }

    public void setNombrel(String nombrel) {
        this.nombrel = nombrel;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public Date getFechaSubida() {
        return fechaSubida;
    }

    public void setFechaSubida(Date fechaSubida) {
        this.fechaSubida = fechaSubida;
    }

    public Date getFechaDeEliminacion() {
        return fechaDeEliminacion;
    }

    public void setFechaDeEliminacion(Date fechaDeEliminacion) {
        this.fechaDeEliminacion = fechaDeEliminacion;
    }

    public double getTamano() {
        return tamano;
    }

    public void setTamano(double tamano) {
        this.tamano = tamano;
    }

    public String getDerechosDeAutor() {
        return derechosDeAutor;
    }

    public void setDerechosDeAutor(String derechosDeAutor) {
        this.derechosDeAutor = derechosDeAutor;
    }

    public String getLicencias() {
        return licencias;
    }

    public void setLicencias(String licencias) {
        this.licencias = licencias;
    }

    public String getEnlace() {
        return enlace;
    }

    public void setEnlace(String enlace) {
        this.enlace = enlace;
    }
   
}
