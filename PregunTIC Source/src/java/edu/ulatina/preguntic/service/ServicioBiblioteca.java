/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ulatina.preguntic.service;

import edu.ulatina.preguntic.model.Biblioteca;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Daniel Garro
 */
public class ServicioBiblioteca extends Servicio{
    
    @Override
    public void conectar() {
        try {
            //STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);
            //STEP 2: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (ClassNotFoundException ex) {
            System.out.println("No se puedo registrar el driver...");
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("No se puedo conectar...");
        }
    }
    
 public void agregarArchivo(Biblioteca b) {

        try {
            this.conectar();
            //STEP 3: Execute a query
            stmt = conn.createStatement();
            String sql;
            sql = "INSERT INTO tbl_archivo (id,idUsuarioCarnet,nombrel,tipoDeArchivo,fechaSubida,fechaDeEliminacion,tamano,derechosDeAutor,licencias,enlace)" + " VALUES (?,?,?,?,?,?,?,?,?,?);";
            paInsertar = conn.prepareStatement(sql);
            paInsertar.setInt(1, b.getId());
            paInsertar.setInt(2, b.getIdUsuarioCarnet());
            paInsertar.setString(3, b.getNombrel());
            paInsertar.setString(4, b.getTipoArchivo());
            paInsertar.setDate(5, b.getFechaSubida());
            paInsertar.setDate(6, b.getFechaDeEliminacion());
            paInsertar.setDouble(7, b.getTamano());
            paInsertar.setString(8, b.getDerechosDeAutor());
            paInsertar.setString(9, b.getLicencias());
            paInsertar.setString(10,b.getEnlace());
       
            paInsertar.executeUpdate();

        } catch (Exception ex) {
            System.out.println("No se pudo subir el documento..");
        } finally {
            try {
                if (!paInsertar.isClosed()) {
                    paInsertar.close();
                }

                if (!stmt.isClosed()) {
                    stmt.close();
                }

                if (!conn.isClosed()) {
                    conn.close();
                }

            } catch (Exception e) {
                System.out.println("\nNo pude cerrar...");
            }
        }
    }
 /*
  public void ModificarArchivo() {

       
        try {
            this.conectar();
            //STEP 3: Execute a query
            stmt = conn.createStatement();
            String sql;
            sql = "UPDATE tbl_usuario SET id=(?), ;
            System.out.println(sql);
        }
  }
    public void buscarArchivo() {
         try {
             this.conectar();
              stmt = conn.createStatement();
            String sql;
            sql = "SELECT nombrel FROM tbl_archivo WHERE nombrel LIKE '%\" + libr + \"%' OR autor LIKE '%\"+ libr +\"%' OR editorial LIKE '%\"+ libr +\"%' OR isbn='\"+ libr +\"' \";"
             rs = stmt.executeQuery(sql);
         }
    }
    
    */
}
